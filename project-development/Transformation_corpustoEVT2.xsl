<?xml version="1.0" encoding="UTF-8"?>
<!-- Transformation à appliquer dans un deuxième temps sur le document en sortie de la première transformation :
    transformation des éléments <rs> et des attributs corresp. -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:xi="http://www.w3.org/2001/XInclude" exclude-result-prefixes="xs tei" version="2.0">
    <xsl:strip-space elements="*"/>
    <xsl:output indent="yes" method="xml" encoding="UTF-8"/>

    <xsl:template match="@* | node()" mode="#all">
        <xsl:choose>
            <xsl:when test="matches(name(.), '^(part|instant|anchored|default|full|status)$')"/>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@* | node()" mode="#current"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="rs">
        <xsl:if test=".[@type = 'person']">
            <xsl:element name="persName">
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@corresp"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:if>
        <xsl:if test=".[@type = 'place']">
            <xsl:element name="placeName">
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@corresp"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:if>
        <xsl:if test=".[@type = 'org']">
            <xsl:element name="orgName">
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@corresp"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:if>
        <xsl:if test=".[@type = 'book']">
            <xsl:element name="bibl">
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@corresp"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:if>
        <xsl:if test=".[@type = 'term']">
            <xsl:element name="term">
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@corresp"/>
                </xsl:attribute>
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="text/persName">
        <xsl:if test="./@corresp">
            <xsl:element name="persName">
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@corresp"/>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="descendant::node()">
                        <xsl:copy-of select="./node()"/>
                    </xsl:when>
                    <xsl:otherwise >
                        <xsl:value-of select="./text()"/>>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="text/placeName">
        <xsl:if test="./@corresp">
            <xsl:element name="placeName">
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@corresp"/>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="descendant::node()">
                        <xsl:copy-of select="./node()"/>
                    </xsl:when>
                    <xsl:otherwise >
                        <xsl:value-of select="./text()"/>>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="text/orgName">
        <xsl:if test="./@corresp">
            <xsl:element name="orgName">
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@corresp"/>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="descendant::node()">
                        <xsl:copy-of select="./node()"/>
                    </xsl:when>
                    <xsl:otherwise >
                        <xsl:value-of select="./text()"/>>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="text/bibl">
        <xsl:if test="./@corresp">
            <xsl:element name="bibl">
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@corresp"/>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="descendant::node()">
                        <xsl:copy-of select="./node()"/>
                    </xsl:when>
                    <xsl:otherwise >
                        <xsl:value-of select="./text()"/>>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="text/term">
        <xsl:if test="./@corresp">
            <xsl:element name="term">
                <xsl:attribute name="ref">
                    <xsl:value-of select="./@corresp"/>
                </xsl:attribute>
                <xsl:choose>
                    <xsl:when test="descendant::node()">
                        <xsl:copy-of select="./node()"/>
                    </xsl:when>
                    <xsl:otherwise >
                        <xsl:value-of select="./text()"/>>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:element>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
