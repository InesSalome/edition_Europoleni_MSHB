This folder contains conversions needed to switch from the corpus encoded according to scientific criteria to a corpus encoded to be well displayed with EVT. The transformation is effective in two steps :
- first we apply `Transformation_corpustoEVT1.xsl` on the file with the `<teiCorpus>`
- then we apply `Transformation_corpustoEVT2.xsl` on the output file from the first transformation.
