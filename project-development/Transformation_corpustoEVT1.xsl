<?xml version="1.0" encoding="UTF-8"?>

<!-- Première transformation à appliquer sur le <teiCorpus> du projet :
    Faire correspondre la structure globale du corpus à la structure lisible par le logiciel EVT. -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0" xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    exclude-result-prefixes="xs tei"
    version="2.0">
    
    <xsl:output method="xml" indent="yes"/>
   
    
    <xsl:template match="teiCorpus">
        <xsl:element name="TEI">
            <xsl:attribute name="xml:id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:apply-templates select="teiHeader"/>
            <xsl:element name="text">
            <xsl:element name="group">
                <xsl:apply-templates select="TEI"/>
            </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="teiHeader">
        <xsl:element name="teiHeader">
            <xsl:copy select="./fileDesc"/>
            <xsl:copy-of select=".//titleStmt"/>
            <xsl:copy-of select=".//publicationStmt"/>
            <xsl:apply-templates select=".//sourceDesc"/>
            <xsl:apply-templates select=".//profileDesc"/>
            <xsl:apply-templates select=".//encodingDesc"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="sourceDesc">
        <xsl:element name="sourceDesc">
        <xsl:copy-of select="msDesc"/>
        <xsl:copy-of select="listBibl[1]"/>
        <xsl:copy-of select="//listPlace"/>
        <xsl:copy-of select="//listPerson"/>
        <xsl:copy-of select="//listOrg"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="profileDesc">
        <xsl:element name="profileDesc">
            <xsl:copy-of select="langUsage"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="encodingDesc">
        <xsl:element name="encodingDesc">
            <xsl:copy-of select="projectDesc"/>
            <xsl:copy-of select="editorialDecl"/>
            <xsl:element name="classDecl">
                <xsl:copy-of select=".//taxonomy[@type='mots-clefs']"/>
                <xsl:element name="taxonomy">
                    <xsl:copy-of select=".//listBibl/*"/>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="TEI">
        <xsl:for-each select=".">
            <xsl:element name="text">
            <xsl:copy-of select="./@xml:id"/>
            <xsl:apply-templates select="teiHeader"/>
            <xsl:apply-templates select="text"/>
            <xsl:apply-templates select="body"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="TEI/teiHeader">
        <xsl:element name="front">
            <xsl:copy select=".//titleStmt">
                <xsl:copy-of select="./title"/>
            </xsl:copy>
            <xsl:copy select=".//publicationStmt">
                <xsl:copy-of select="./idno"/>
            </xsl:copy>
            <xsl:copy-of select=".//sourceDesc"/>
            <xsl:copy-of select=".//profileDesc"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="TEI/text">
        <xsl:copy-of select="./body"/>
    </xsl:template>
    
</xsl:stylesheet>
