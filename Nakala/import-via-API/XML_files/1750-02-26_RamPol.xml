<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="out/ODD_Europoleni.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"? schematypens="http://purl.oclc.org/dsdl/schematron"? xmlns:xi="http://www.w3.org/2001/XInclude">
<?xml-stylesheet type="text/css" href="Customisation_mode_auteur.css"?>

<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="RAM_POL_1750_02_26">

    <teiHeader>
        <!-- Métadonnées de la lettre en question. -->
        <fileDesc>
            <titleStmt>
                <!-- Mettre l'entrée de la lettre dans l'index comme elle apparaîtra sur le site. -->
                <title>Lettre de Jean-Philippe Rameau à Giovanni Poleni, Paris, 26 février
                    1750.</title>
                <!-- Chercheurs qui ont travaillé sur la lettre. Qui a fait quoi. Associer un identifiant à la personne pour pouvoir faire les liens vers les notes et commentaires dont elle est l'auteur. -->
                <respStmt>
                    <resp>Encodage</resp>
                    <persName corresp="CH">Cécile Hamon</persName>
                </respStmt>
                <respStmt>
                    <resp>Vérification encodage</resp>
                    <persName corresp="GP">Gwenaëlle Patat</persName>
                </respStmt>
            </titleStmt>
            <publicationStmt>
                <idno xml:base="URI absolue pointant vers donnée" type="DOI">identifiant de la donnée dans Nakala</idno>
                <!-- Pas besoin de répéter les indications mises dans les métadonnées du projet, on peut se contenter de ce qui est indiqué si-dessous. -->
                <p>cf. supra</p>
            </publicationStmt>
            <!-- Informations relatives à la source qui sert de base à l'édition électronique, manuscrite ou numérisée. -->
            <sourceDesc>
                <!-- Pour une source manuscrite, rappeler sa cote et où elle est conservée. -->
                <msDesc>
                    <msIdentifier>
                        <country>Italie</country>
                        <settlement>Vérone</settlement>
                        <repository>Biblioteca civica</repository>
                        <idno>3096E f° 539</idno>
                    </msIdentifier>
                    <!-- Pour la description matérielle : couleur de l'encre ; enveloppe ; dimensions ; etc., selon les informations dont on dispose. -->
                    <!-- Dans le cas de la présence d'un brouillon et d'une version orginale envoyée, étant donné que l'on traduit et transcrit la version originale, mettre la description de cette version et non pas du brouillon. -->
                    <physDesc>
                        <objectDesc form="letter">
                            <!-- Type de support matériel -->
                            <supportDesc material="paper">
                                <support>Papier</support>
                                <!-- Nombre et dimensions du feuillet : optionnels. -->
                                <extent>1 folio</extent>
                            </supportDesc>
                        </objectDesc>
                        <!-- Indiquer la couleur de l'encre. Se mettre d'accord sur une typologie pour l'@medium : brown-ink, red-ink, etc. -->
                        <handDesc>
                            <handNote medium="brown-ink">De la main de Jean-Philippe
                                Rameau.</handNote>
                        </handDesc>
                        <!-- Indiquer la présence d'une enveloppe s'il y'en a une. -->
                        <accMat>Enveloppe</accMat>
                    </physDesc>
                    <additional>
                        <!-- Mettre ici les références bibliographiques liées à la lettre en question. -->
                        <surrogates>
                            <!-- On peut choisir un <bibl> ou bien un <biblStruct>, format davantage structuré, qui est généré automatiquement depuis l'export TEI de Zotero. -->
                            <bibl>A.-M. Chouillet, « Le concept de beauté dans les écrits théoriques
                                de Rameau, in Rameau en Auvergne », Recueil d'études établi et
                                présenté par Jean-Louis Jam, Clermont-Ferrand, 1986, p.
                                101-103</bibl>
                        </surrogates>
                    </additional>
                </msDesc>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <correspDesc>
                <!-- Dans <correspAction>, mettres les infos d'envoie et de réception. Ne pas mettre le lieu et la date si on les connaît pas.-->
                <correspAction type="sent">
                    <persName corresp="#Rameau">RAMEAU, Jean-Philippe</persName>
                    <settlement>Paris</settlement>
                    <date when="1750-02-26">26 février 1750</date>
                </correspAction>
                <correspAction type="received">
                    <persName corresp="#Poleni">POLENI, Giovanni</persName>
                    <settlement>Padoue</settlement>
                </correspAction>
                <!-- Dans <correspContext>, mettre les flux dans laquelle la lettre s'inscrit : à quelle lettre elle répond ? Quelle lettre la suit ? On peut faire un @corresp qui pointe vers la lettre précédente ou suivante en se référant à leur xml:id. Rester prudent sur les lettres précédentes et suivantes (cf. cas des lettres intermédiaires). -->
                <correspContext>
                    <!--<ref type="prev"
                        corresp="#date3PremieresLettresEmettereur3PremieresLettresRecepteur">Lettre
                        précédente de <persName corresp="#Poleni">??</persName> à <persName
                            corresp="#Hermann">??</persName>: <date when="1731">1731?</date>
                    </ref>-->
                    <ref type="next" corresp="#POL_RAM_1750_08_30">Lettre suivante de <persName
                            corresp="#Poleni">Giovanni Poleni</persName> à <persName
                            corresp="#Rameau">Jean-Philippe Rameau</persName>: <date
                            when="1750-08-30">30 août 1750</date>
                    </ref>
                </correspContext>
            </correspDesc>
            <!-- Résumé du contenu apparaissant dans la notice.-->
            <abstract>
                <p>Envoi de son traité <bibl
                        corresp="#Reflexions_sur_Demonstration_Principe_de_l_Harmonie"
                        xml:base="https://gallica.bnf.fr/ark:/12148/bpt6k1082246.r=Démonstration+du+principe+de+l%27harmonie.langFR"
                            ><hi rend="italic">Démonstration du principe de l'harmonie servant de
                            base à tout l'art musical théorique et pratique</hi></bibl>, « Approuvée
                    par Messieurs de <orgName corresp="#AcadRSciencesFr">l'Académie Royale des
                        Sciences</orgName> , et dédiée à <persName corresp="#Argenson">Monseigneur
                        le Comte d’Argenson</persName>, Ministre et Secrétaire d'Etat ».</p>
            </abstract>
            <textClass>
                <!-- Liste de balises <term> et renvoie vers des termes indexés si besoin.-->
                <keywords>
                    <!-- Distinguer "musique" et "art musical" dans les mots-clés ? -->
                    <term corresp="#musique">musique</term>
                    <term corresp="#art_musical">art musical</term>
                    <term corresp="#harmonie">harmonie</term>
                    <term corresp="#mathematiques">mathématiques</term>
                </keywords>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change when="2021-04-09" who="CH">Proposition de l'encodage</change>
            <change when="2021-04-09" who="GP">Première relecture de l'encodage</change>
            <change when="2021-04-12" who="CH">Correction de l'encodage</change>
            <change when="2021-04-16" who="CH">Relève des noms d'ouvrages et inscriptions dans
                l'index du teicorpus, balisage des titres en italique avec
                    <att>rend</att><val>italic</val> et des mots en italique ou soulignés avec la
                balise <gi>hi</gi> et les attributs <att>rend</att><val>italic</val> et
                    <att>rend</att><val>underline</val>.</change>
            <change when="2021-04-21" who="GP">Relecture et correction des modifications de
                l'encodage</change>
            <change when="2021-04-21" who="GP">Relecture et correction des modifications de
                l'encodage</change>
            <change when="2021-04-27" who="CH">Dernières modifications de l'encodage</change>
            <change when="2021-05-03" who="GP">Relecture des dernières modifications de
                l'encodage</change>
        </revisionDesc>
    </teiHeader>

    <!-- Corps de la lettre. -->
    <text>
        <body>
            <div type="transcription" xml:lang="fr">
                <pb xml:id="RamPol_1_1750-02-26" n="Transcription_1750-02-26_RamPol_1"
                    facs="data/images/single/1750-02-26_RamPol/1750-02-26_RamPol_1.jpg"
                    type="recto"/>
                <opener>
                    <salute>
                        <rs type="person" corresp="#Poleni">Monsieur</rs>
                    </salute>
                </opener>
                <p>Il a été un tems où la démonstration que j’ai l’honneur de vous envoyer auroit
                    comblé les souhaits de tous les philosophes ; qu’est-ce qui pouvoit, en effet,
                    les engager à tant de recherches dans la <term corresp="#musique"
                    >musique</term>, si ce n’étoit le désir d’en découvrir le principe ? Bien moins
                    philosophe que musicien je ne songeois qu’aux moyens de faciliter la pratique de
                    mon <term corresp="#art_musical">Art</term> , lorsqu’avec son principe j’ai
                    découvert dans la nature celui qui sert de base à toutes les <term
                        corresp="#mathematiques">mathématiques</term>, si je ne me trompe, je ne
                    connois bien que la <term corresp="#musique">musique</term> ; c’est à vous, <rs
                        type="person" corresp="#Poleni">Monsieur</rs> , d’en juger, et de donner de
                    justes bornes à mes idées.</p>
                <closer>Je suis avec respect,<lb/>
                    <lb/><rs type="person" corresp="#Poleni">Monsieur</rs>
                    <signed>
                        <lb/><rs type="person" corresp="#Rameau">Votre très humble, et très
                            obéissant serviteur</rs>
                        <persName corresp="#Rameau">Rameau</persName>
                    </signed>
                    <dateline><lb/>A Paris ce 26 février 1750.</dateline>
                </closer>
            </div>
        </body>
    </text>

</TEI>
