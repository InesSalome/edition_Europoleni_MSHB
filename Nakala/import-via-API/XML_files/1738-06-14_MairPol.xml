<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="out/ODD_Europoleni.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"? schematypens="http://purl.oclc.org/dsdl/schematron"? xmlns:xi="http://www.w3.org/2001/XInclude">
<?xml-stylesheet type="text/css" href="Customisation_mode_auteur.css"?>


<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="MAIR_POL_1738_06_14">

    <teiHeader>
        <!-- Métadonnées de la lettre en question. -->
        <fileDesc>
            <titleStmt>
                <!-- Mettre l'entrée de la lettre dans l'index comme elle apparaîtra sur le site. -->
                <title>Lettre de Jean-Jacques Dortous de Mairan à Giovanni Poleni, Paris, 14 juin
                    1738.</title>
                <!-- Chercheurs qui ont travaillé sur la lettre. Qui a fait quoi. Associer un identifiant à la personne pour pouvoir faire les liens vers les notes et commentaires dont elle est l'auteur. -->
                <respStmt>
                    <resp>Encodage</resp>
                    <persName corresp="CH">Cécile Hamon</persName>
                </respStmt>
                <respStmt>
                    <resp>Vérification encodage</resp>
                    <persName corresp="GP">Gwenaëlle Patat</persName>
                </respStmt>
            </titleStmt>
            <publicationStmt>
                <idno xml:base="URI absolue pointant vers donnée" type="DOI">identifiant de la donnée dans Nakala</idno>
                <!-- Pas besoin de répéter les indications mises dans les métadonnées du projet, on peut se contenter de ce qui est indiqué si-dessous. -->
                <p>cf. supra</p>
            </publicationStmt>
            <!-- Informations relatives à la source qui sert de base à l'édition électronique, manuscrite ou numérisée. -->
            <sourceDesc>
                <!-- Pour une source manuscrite, rappeler sa cote et où elle est conservée. -->
                <msDesc>
                    <msIdentifier>
                        <country>Italie</country>
                        <settlement>Vérone</settlement>
                        <repository>Biblioteca civica</repository>
                        <idno>3096 E, f°387</idno>
                    </msIdentifier>
                    <!-- Pour la description matérielle : couleur de l'encre ; enveloppe ; dimensions ; etc., selon les informations dont on dispose. -->
                    <!-- Dans le cas de la présence d'un brouillon et d'une version orginale envoyée, étant donné que l'on traduit et transcrit la version originale, mettre la description de cette version et non pas du brouillon. -->
                    <physDesc>
                        <!-- Draft, copy or letter? -->
                        <objectDesc form="draft">
                            <!-- Type de support matériel -->
                            <supportDesc material="paper">
                                <support>Papier</support>
                                <!-- Dimensions du feuillet : optionnelles. -->
                            </supportDesc>
                        </objectDesc>
                        <!-- Indiquer la couleur de l'encre. Se mettre d'accord sur une typologie pour l'@medium : brown-ink, red-ink, etc. -->
                        <handDesc>
                            <handNote medium="brown-ink">De la main de Jean-Jacques Dortous de
                                Mairan.</handNote>
                        </handDesc>
                    </physDesc>
                    <additional>
                        <!-- Mettre ici les références bibliographiques liées à la lettre en question. -->
                        <surrogates>
                            <!-- On peut choisir un <bibl> ou bien un <biblStruct>, format davantage structuré, qui est généré automatiquement depuis l'export TEI de Zotero. -->
                            <p>
                                <bibl/>
                            </p>
                        </surrogates>
                    </additional>
                </msDesc>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <correspDesc>
                <!-- Dans <correspAction>, mettres les infos d'envoie et de réception. Ne pas mettre le lieu et la date si on les connaît pas.-->
                <correspAction type="sent">
                    <persName corresp="#Mairan">DORTOUS DE MAIRAN, Jean-Jacques</persName>
                    <settlement>Paris</settlement>
                    <date when="1738-06-14">14 juin 1738</date>
                </correspAction>
                <correspAction type="received">
                    <persName corresp="#Poleni">POLENI, Giovanni</persName>
                </correspAction>
                <!-- Dans <correspContext>, mettre les flux dans laquelle la lettre s'inscrit : à quelle lettre elle répond ? Quelle lettre la suit ? On peut faire un @corresp qui pointe vers la lettre précédente ou suivante en se référant à leur xml:id. Rester prudent sur les lettres précédentes et suivantes (cf. cas des lettres intermédiaires). -->
                <!--<correspContext>
                    <ref type="prev"
                        corresp="#date3PremieresLettresEmettereur3PremieresLettresRecepteur">Lettre
                        précédente de <persName corresp="#Poleni">??</persName> à <persName
                            corresp="#Hermann">??</persName>: <date when="1731">1731?</date>
                    </ref>
                    <ref type="next"
                        corresp="#date3PremieresLettresEmettereur3PremieresLettresRecepteur">Lettre
                        suivante de <persName corresp="#Hermann">??</persName> à <persName
                            corresp="#Poleni">??</persName>: <date when="1731-11">Novembre
                            1731?</date>
                    </ref>
                </correspContext>-->
            </correspDesc>
            <!-- Résumé du contenu apparaissant dans la notice.-->
            <abstract>
                <p/>
            </abstract>
            <textClass>
                <!-- Liste de balises <term> et renvoie vers des termes indexés si besoin.-->
                <keywords>
                    <term></term>
                </keywords>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change when="2021-04-09" who="CH">Proposition de l'encodage</change>
            <change when="2021-04-09" who="GP">Première relecture de l'encodage</change>
            <change when="2021-04-12" who="CH">Correction de l'encodage</change>
            <change when="2021-04-16" who="CH">Relève des noms d'ouvrages et inscriptions dans
                l'index du teicorpus, balisage des titres en italique avec
                    <att>rend</att><val>italic</val> et des mots en italique ou soulignés avec la
                balise <gi>hi</gi> et les attributs <att>rend</att><val>italic</val> et
                    <att>rend</att><val>underline</val>.</change>
            <change when="2021-04-21" who="GP">Relecture et correction des modifications de
                l'encodage</change>
            <change when="2021-04-21" who="GP">Relecture et correction des modifications de
                l'encodage</change>
            <change when="2021-04-27" who="CH">Dernières modifications de l'encodage</change>
            <change when="2021-05-03" who="GP">Relecture des dernières modifications de
                l'encodage</change>
        </revisionDesc>
    </teiHeader>

    <!-- Corps de la lettre. -->
    <text>
        <body>
            <div type="transcription" xml:lang="fr">
                <pb xml:id="MairPol_1_1738-06-14" n="Transcription_1738-06-14_MairPol_1"
                    facs="data/images/single/1738-06-14_MairPol/1738-06-14_MairPol_1.jpg"
                    type="recto"/>
                <opener>
                    <salute><rs type="person" corresp="#Poleni">Monsieur</rs>,</salute>
                </opener>
                <p>J’étais à la campagne lorsque votre première lettre dont vous m’avez honoré est
                    arrivée en ce pays, et ce n’est que depuis peu de jours que j’ai reçu la
                    seconde, toutes les deux étant du mois de mai dernier. Les livres que vous
                    souhaitez, dans l’une et l’autre, des petits catalogues inclus dans ces lettres,
                    seront prêts incessamment. Ce qu’il y a de plus difficile c’est de les envoyer à
                        <placeName corresp="#Turin">Turin</placeName> à l’adresse que vous m’avez
                    indiquée. Cependant, <persName corresp="#Guerin">le sieur Guérin</persName>
                    Libraire dont je me sers ordinairement et dont l’exactitude et la bonne foi me
                    sont connues, m’a assuré qu’il aurait bientôt le moyen de les faire partir. Nous
                    aurons peut-être le temps de sçavoir votre première résolution, <rs
                        type="person" corresp="#Poleni">Monsieur</rs>, touchant les anciens mémoires
                    de <rs type="org" corresp="#AcadRSciencesFr">l’Académie</rs> et les machines
                    dont vous me demandez le prix, et en ce cas, on ne ferait qu’un paquet du tout,
                    ce qui serait beaucoup plus commode. <pb xml:id="MairPol_2_1738-06-14"
                        n="Transcription_1738-06-14_MairPol_2"
                        facs="data/images/single/1738-06-14_MairPol/1738-06-14_MairPol_2.jpg"
                        type="recto"/> Les anciens mémoires nouvellement imprimés y compris les 4
                    volumes de Tables des matières font en tout 18 volumes, et vous coûteront 180 £
                    à raison de 10 £ le volume. Ils le vendent ordinairement 12 £. Mais comme j’ai
                    procuré en partie cette édition au libraire du nombre desquels est <persName
                        corresp="#Guerin">le Sieur Guérin</persName>, j’espère les avoir à ce prix.
                    Les 6 volumes de Machines seront de 92 £, le tout en feuilles. Ce sont les <bibl
                        corresp="#Machines_Approuvees_par_l_Academie_Tome_6">
                        <hi rend="italic">Machines approuvées par l’Académie</hi></bibl> dont la
                    plupart sont indiquées à la fin de l’histoire de chacun des volumes que <rs
                        type="org" corresp="#AcadRSciencesFr">l’Académie</rs> fait imprimer tous les
                    ans. Il y a 420 ou 430 planches. Quant aux avances, <rs type="person"
                        corresp="#Poleni">Monsieur</rs>, je vous prie de ne point vous en
                    embarrasser, ce sera après le balot fait, que je vous marquerai la somme du
                    total. Et si cela se trouve cependant que je serai à la campagne, je laisserai
                    ici de si bons ordres pour que le paquet soit bien fait que j’espère que vous
                    recevrez le tout en bon état. Je n’y mettrai point le livre des observations
                    faites au Nord pour la mesure de la terre, parce que <persName
                        corresp="#Maupertuis">Mr de Maupertuis</persName> m’en <pb xml:id="MairPol_3_1738-06-14"
                            n="Transcription_1738-06-14_MairPol_3"
                            facs="data/images/single/1738-06-14_MairPol/1738-06-14_MairPol_3.jpg"
                            type="recto"/> a donné un exemplaire pour
                    vous, <rs type="person" corresp="#Poleni">Monsieur</rs>, je le remis, il y a
                    quelques jours à <persName corresp="#Polignac">M. Le Cardinal de
                        Polignac</persName> qui voulut bien le charger de vous le faire tenir.</p>
                <p>Je remis aussi il y a environ deux mois un exemplaire entier des pièces du prix
                    1737 avec 6 exemplaires particuliers de la vôtre. C’est tout ce je puis obtenir
                    de <rs type="org" corresp="AcadRSciencesFr">l’Académie</rs> ; et si vous
                    souhaitiez d’en acheter du libraire, il ne vous donnerait pas votre pièce sans
                    les autres qui l’accompagnent. J’en viens <rs type="person" corresp="#Poleni"
                        >Monsieur</rs>, à l’article le plus intéressant pour moi et pour <rs
                        type="org" corresp="AcadRSciencesFr">l’Académie</rs> ; touchant à la place
                    d’Associé étranger. Croyez, je vous en supplie, que je me ferai un sensible
                    plaisir de travailler à procurer cet avantage à la Compagnie, et à me procurer
                    en même temps l’honneur de devenir votre confrère et que j’y travaillerai avec
                    zèle, lorsque l’occasion s’en présentera. Car personne au monde ne vous honore
                    davantage, et n’est avec plus d’estime et de respect que je suis,</p>
                <closer>
                    <lb/><rs type="person" corresp="#Poleni">Monsieur</rs>
                    <signed><lb/><rs type="person" corresp="#Mairan">Votre très humble et très
                            obéissant serviteur</rs><lb/><persName corresp="#Mairan">Dortous de
                            Mairan</persName>
                    </signed>
                    <dateline>À Paris ce 14 juin 1738.</dateline>
                </closer>
                <pb xml:id="MairPol_4_1738-06-14" n="Transcription_1738-06-14_MairPol_4"
                    facs="data/images/single/1738-06-14_MairPol/1738-06-14_MairPol_4.jpg"
                    type="verso"/>
                <postscript>
                    <p>PS : Je suis ravi de l’heureux retour de <persName corresp="#Maffei">M. le
                            Marq. Maffei</persName> , et j’ose vous supplier <rs type="person"
                            corresp="#Poleni">Monsieur</rs> , lorsque vous le verrez, de vouloir
                        l’assurer de mes respects.</p>
                </postscript>
            </div>
        </body>
    </text>

</TEI>
