<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="out/ODD_Europoleni.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"? schematypens="http://purl.oclc.org/dsdl/schematron"? xmlns:xi="http://www.w3.org/2001/XInclude">
<?xml-stylesheet type="text/css" href="Customisation_mode_auteur.css"?>

<!DOCTYPE symboles SYSTEM "symboles.dtd">

<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="POL_RAM_1750_08_30">

    <teiHeader>
        <!-- Métadonnées de la lettre en question. -->
        <fileDesc>
            <titleStmt>
                <!-- Mettre l'entrée de la lettre dans l'index comme elle apparaîtra sur le site. -->
                <title>Lettre de Giovanni Poleni à Jean-Philippe Rameau, Padoue, le 30 août
                    1750.</title>
                    <!-- Chercheurs qui ont travaillé sur la lettre. Qui a fait quoi. Associer un identifiant à la personne pour pouvoir faire les liens vers les notes et commentaires dont elle est l'auteur. -->
                <respStmt>
                    <resp>Encodage</resp>
                    <persName corresp="CH">Cécile Hamon</persName>
                </respStmt>
                <respStmt>
                    <resp>Vérification encodage</resp>
                    <persName corresp="GP">Gwenaëlle Patat</persName>
                </respStmt>
            </titleStmt>
            <publicationStmt>
                <idno xml:base="URI absolue pointant vers donnée" type="DOI">identifiant de la donnée dans Nakala</idno>
                <!-- Pas besoin de répéter les indications mises dans les métadonnées du projet, on peut se contenter de ce qui est indiqué si-dessous. -->
                <p>cf. supra</p>
            </publicationStmt>
            <!-- Informations relatives à la source qui sert de base à l'édition électronique, manuscrite ou numérisée. -->
            <sourceDesc>
                <!-- Pour une source manuscrite, rappeler sa cote et où elle est conservée. -->
                <msDesc>
                    <msIdentifier>
                        <country>Italie</country>
                        <settlement>Vérone</settlement>
                        <repository>Biblioteca civica</repository>
                        <idno>3096E f° 541</idno>
                    </msIdentifier>
                    <!-- Pour la description matérielle : couleur de l'encre ; enveloppe ; dimensions ; etc., selon les informations dont on dispose. -->
                    <!-- Dans le cas de la présence d'un brouillon et d'une version orginale envoyée, étant donné que l'on traduit et transcrit la version originale, mettre la description de cette version et non pas du brouillon. -->
                    <physDesc>
                        <objectDesc form="draft">
                            <!-- Type de support matériel -->
                            <supportDesc material="paper">
                                <support>Papier</support>
                                <!-- Dimensions du feuillet : optionnelles. -->
                                <extent>1 folio</extent>
                            </supportDesc>
                        </objectDesc>
                        <!-- Indiquer la couleur de l'encre. Se mettre d'accord sur une typologie pour l'@medium : brown-ink, red-ink, etc. -->
                        <handDesc>
                            <handNote medium="brown-ink">De la main de Giovanni Poleni.</handNote>
                        </handDesc>
                        <!-- Indiquer la présence d'une enveloppe s'il y'en a une. -->
                        <accMat>Enveloppe</accMat>
                    </physDesc>
                    <additional>
                        <!-- Mettre ici les références bibliographiques liées à la lettre en question. -->
                        <surrogates>
                            <!-- On peut choisir un <bibl> ou bien un <biblStruct>, format davantage structuré, qui est généré automatiquement depuis l'export TEI de Zotero. -->
                            <p>
                                <bibl></bibl>
                            </p>
                        </surrogates>
                    </additional>
                </msDesc>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <correspDesc>
                <!-- Dans <correspAction>, mettres les infos d'envoie et de réception. Ne pas mettre le lieu et la date si on les connaît pas.-->
                <correspAction type="sent">
                    <persName corresp="#Poleni">POLENI, Giovanni</persName>
                    <settlement>Padoue</settlement>
                    <date when="1750-08-30">30 août 1750</date>
                </correspAction>
                <correspAction type="received">
                    <persName corresp="#Rameau">RAMEAU, Jean-Philippe</persName>
                    <settlement>Paris</settlement>
                </correspAction>
                <!-- Dans <correspContext>, mettre les flux dans laquelle la lettre s'inscrit : à quelle lettre elle répond ? Quelle lettre la suit ? On peut faire un @corresp qui pointe vers la lettre précédente ou suivante en se référant à leur xml:id. Rester prudent sur les lettres précédentes et suivantes (cf. cas des lettres intermédiaires). -->
                <correspContext>
                    <ref type="prev" corresp="#RAM_POL_1750_02_26">Lettre précédente de <persName
                            corresp="#Rameau">Jean-Philippe Rameau</persName> à <persName
                            corresp="#Poleni">Giovanni Poleni</persName>: <date when="1750-02-26">26
                            février 1750</date>
                    </ref>
                    <!--<ref type="next"
                        corresp="#date3PremieresLettresEmettereur3PremieresLettresRecepteur">Lettre
                        suivante de <persName corresp="#Hermann">??</persName> à <persName
                            corresp="#Poleni">??</persName>: <date when="1731-11">??</date>
                    </ref>-->
                </correspContext>
            </correspDesc>
            <!-- Résumé du contenu apparaissant dans la notice.-->
            <abstract>
                <p>Réponse à l’envoi du traité de <persName corresp="#Rameau">Rameau</persName>,
                        <bibl corresp="#demonstration_principe_harmonie"><hi rend="italic">Démonstration du principe de l'harmonie servant de base à tout l'art
                        musical théorique et pratique</hi></bibl></p>
            </abstract>
            <textClass>
                <!-- Liste de balises <term> et renvoie vers des termes indexés si besoin.-->
                <keywords>
                    <!-- Doubler les entrées "musique" et "art musical" ou les regrouper sous le même mot-clé ? -->
                    <term corresp="#musique">musique</term>
                    <term corresp="#art_musical">art musical</term>
                    <term corresp="*#harmonie">harmonie</term>
                    <term><orgName corresp="#AcadRSciencesFr">Académie
                            des sciences de Paris</orgName></term>
                </keywords>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change when="2021-04-09" who="CH">Proposition de l'encodage</change>
            <change when="2021-04-09" who="GP">Première relecture de l'encodage</change>
            <change when="2021-04-12" who="CH">Correction de l'encodage</change>
            <change when="2021-04-16" who="CH">Relève des noms d'ouvrages et inscriptions dans l'index du
                teicorpus, balisage des titres en italique avec <att>rend</att><val>italic</val> et des
                mots en italique ou soulignés avec la balise <gi>hi</gi> et les attributs
                <att>rend</att><val>italic</val> et <att>rend</att><val>underline</val>.</change>
            <change when="2021-04-21" who="GP">Relecture et correction des modifications de l'encodage</change>
            <change when="2021-04-21" who="GP">Relecture et correction des modifications de l'encodage</change>
            <change when="2021-04-27" who="CH">Dernières modifications de l'encodage</change>
            <change when="2021-05-03" who="GP">Relecture des dernières modifications de l'encodage</change>
        </revisionDesc>
    </teiHeader>

    <!-- Corps de la lettre. -->
    <text>
        <body>
            <div type="transcription" xml:lang="lat">
                <pb xml:id="PolRam_1_1750-08-30" n="Transcription_1750-08-30_PolRam_1"
                    facs="data/images/single/1750-08-30_PolRam/1750-08-30_PolRam_1.jpg"
                    type="recto"/>
                <opener>
                    <salute>
                        <lb/><persName corresp="#Rameau">Clarissimo Ornatissimoque Viro
                        <lb/>Dno Rameau</persName></salute>
                    <signed><lb/><persName corresp="#Poleni">Joannes Polenus
                        S.P.D.</persName></signed>
                </opener>
                <p>Puderet me tam serò ad Litteras quas rescribere : sed velim, qua humanitate es,
                    persuadeas Tibi quod reapse contigit, nempe serò in manus meas pervenisse illas,
                    <!-- Encoder Librum Principiorum Harmoniae comme un ouvrage ? --> ac Librum <hi
                        rend="underline">Principiorum Harmoniae</hi> . Pro illis autem atque pro hoc
                    gratias Tibi maximas ago, atque sic habeto, ut si aliqua in re inservire Tibi
                    possim certè mandatis tuis lubens sim obtemperaturus. Librum eum a Te mihi tam
                    benignè missum studiosè legi : quid senserim, cur dicam verbis aliis, quàm iis,
                    quibus usi sunt Viri illi summi, ornamenta seculi nostri, <hi rend="underline"
                        >de <persName corresp="#Mairan">Mairan</persName></hi> , et <hi
                        rend="underline"><persName corresp="#Alembert">d’Alembert</persName></hi>?
                    Id ego quidem iudicium de tuo Opere concepi, quod illi priùs fecerant, et ad
                    extremum <hi rend="underline">Excerpti ex Commentariis <orgName
                            corresp="#AcadRSciencesFr">Academiae Regiae Scientiarum</orgName></hi>
                    legitur. Certè Operi illi tuo, ob doctas perquisitiones et inventiones,
                    suffragia Philosophorum, laudesque debentur. Ego verò, ingenuam hanc mentis
                    animique mei significationem Tibi notam esse cupiebam. Itaque veritati, mihique
                    satisfeci. Vale.</p>
                <closer>
                    <dateline>Patavio. III. Kal. Septemb. CI&c_inverse;I&c_inverse;CCL</dateline>
                </closer>
            </div>
        </body>
    </text>

</TEI>
